package presentation.servlet.work;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import business.service.WorkService;

/**
 * {@link DBWorkServlet}
 */
@WebServlet("/db_work")
public class WorkServlet extends HttpServlet {


	WorkService service = new WorkService();


	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("dbWorks", service.findAll());


		String view = "/WEB-INF/view/work/dbWork.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		dispatcher.forward(request, response);
	}

}