package presentation.servlet.work;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@link DBWorkInsertServlet}
 */
@WebServlet("/db_work_insert")
public class WorkInsertServlet extends WorkServlet {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		/*
		 * DBWorkServletクラスでインスタンスserviceを定義
		 *
		 * DBWorkServletクラスのinsert()メソッドを実行
		 *
		 */

		request.setCharacterEncoding("UTF-8");
		service.insert(request.getParameter("name"));
		doGet(request, response);
	}

}