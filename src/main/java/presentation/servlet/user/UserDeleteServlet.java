package presentation.servlet.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@link DBWorkDeleteServlet}
 */
@WebServlet("/user_delete")
public class UserDeleteServlet extends UserServlet {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		service.delete(
				Integer.parseInt(request.getParameter("id"))
				);
		doGet(request, response);
	}

}