package business.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;

import business.entity.UserEntity;

/**
 * {@link WorkDao}
 */
public class UserDao {

	public EntityManager createEntityManager() {
		return Persistence.createEntityManagerFactory("webappdb").createEntityManager();
	}

	/**
	 * 全件取得
	 */
	public List<UserEntity> findAll() {
		EntityManager entityManager = createEntityManager();
		CriteriaQuery<UserEntity> query = entityManager.getCriteriaBuilder().createQuery(UserEntity.class);
		return entityManager.createQuery(query.select(query.from(UserEntity.class))).getResultList();
	}

	/**
	 * 取得
	 * @param id ID
	 */
	public UserEntity find(int id) {
		EntityManager entityManager = createEntityManager();
		return entityManager.find(UserEntity.class, id);
	}

	/**
	 * 登録
	 * @param UserEntity user
	 * @return 登録件数
	 */
	public int insert(UserEntity user) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.persist(user);
		entityTransaction.commit();
		return 1;
	}

	/**
	 * 更新
	 * @param dbWork dbWork
	 * @return 更新件数
	 */
	public int update(UserEntity user) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.merge(user);
		entityTransaction.commit();
		return 1;
	}

	/**
	 * 削除
	 * @param dbWork dbWork
	 * @return 削除件数
	 */
	public int delete(UserEntity user) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.remove(entityManager.find(UserEntity.class, user.getId()));
		entityTransaction.commit();
		return 1;
	}

}

