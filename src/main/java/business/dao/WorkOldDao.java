package business.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.entity.WorkEntity;

/**
 * {@link DBWorkDaoJDBC}
 */
/**
 * @author nakaz
 *
 */
public class WorkOldDao {

	/**
	 * DB接続
	 */
	private Connection createConnection() throws ClassNotFoundException, SQLException {

		String dbUrl = "jdbc:mysql://localhost:3306/WEB_APP_DB";
		String dbUser = "root";
		String dbPassword = "Nakazawa0320@";

		// JDBCクラスをロード
		Class.forName("com.mysql.cj.jdbc.Driver");

		return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
	}

	/**
	 * DBWork を全件取得します
	 */
	public List<WorkEntity> findAll() {

		// DBコネクション生成
		try (Connection connection = createConnection()) {
			// SQL実行オブジェクト生成
			PreparedStatement pstmt = connection.prepareStatement("SELECT ID, NAME, VERSION FROM DB_WORK;");
			// SQL実行
			ResultSet rs = pstmt.executeQuery();
			// SQL実行結果からデータを取得
			List<WorkEntity> list = new ArrayList<>();
			while (rs.next()) {
				int id = rs.getInt("ID");
				String name = rs.getString("NAME");
				int version = rs.getInt("VERSION");
				list.add(new WorkEntity(id, name, version));
			}
			return list;
		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * DBWorkを取得
	 * @param id ID
	 */
	public WorkEntity find(int id) {

		try(Connection connection = createConnection()){

			//IDから情報を特定
			PreparedStatement pstmt = connection.prepareStatement("SELECT ID, NAME, VERSION FROM DB_WORK WHERE ID = ?;");

			pstmt.setInt(1, id);

			ResultSet rs = pstmt.executeQuery();

			WorkEntity dbWork = null;

			//特定した情報からNAME, VERSIONを抽出
			while(rs.next()) {
				String name = rs.getString("NAME");
				int version = rs.getInt("VERSION");
				dbWork = new WorkEntity(id, name, version);
			}

			//ID, NAME, VERSIONのセットのインスタンスを返す
			return dbWork;

		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}



	/**
	 * DBWorkを登録
	 * @param dbWork dbWork
	 * @return 登録件数
	 */
	public int insert(WorkEntity dbWork) {

		try(Connection connection = createConnection()){

			PreparedStatement pstmt = connection.prepareStatement("INSERT INTO DB_WORK(NAME) VALUES(?);");

			pstmt.setString(1, dbWork.getName());

			return pstmt.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * DBWorkを更新
	 * @param dbWork dbWork
	 * @return 更新件数
	 */
	public int update(WorkEntity dbWork) {

		try(Connection connection = createConnection()){

			PreparedStatement pstmt = connection.prepareStatement("UPDATE DB_WORK SET NAME = ?, VERSION = VERSION + 1 WHERE ID = ?;");

			// ?の位置に値をセットする
			pstmt.setString(1, dbWork.getName());
			pstmt.setInt(2, dbWork.getId());


			return pstmt.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * DBWorkを削除
	 * @param dbWork dbWork
	 * @return 削除件数
	 */
	public int delete(WorkEntity dbWork) {

		try(Connection connection = createConnection()){

			PreparedStatement pstmt = connection.prepareStatement("DELETE FROM DB_WORK WHERE ID = ? AND VERSION = ?;");


			pstmt.setInt(1, dbWork.getId());
			pstmt.setInt(2, dbWork.getVersion());


			return pstmt.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException(e);
		}
	}

}


