package business.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;

import business.entity.WorkEntity;

/**
 * {@link WorkDao}
 */
public class WorkDao {

	public EntityManager createEntityManager() {
		return Persistence.createEntityManagerFactory("webappdb").createEntityManager();
	}

	/**
	 * DBWork を全件取得します
	 */
	public List<WorkEntity> findAll() {
		EntityManager entityManager = createEntityManager();
		CriteriaQuery<WorkEntity> query = entityManager.getCriteriaBuilder().createQuery(WorkEntity.class);
		return entityManager.createQuery(query.select(query.from(WorkEntity.class))).getResultList();
	}

	/**
	 * DBWork を取得します
	 * @param id ID
	 */
	public WorkEntity find(int id) {
		EntityManager entityManager = createEntityManager();
		return entityManager.find(WorkEntity.class, id);
	}

	/**
	 * DBWork を登録します
	 * @param dbWork dbWork
	 * @return 登録件数
	 */
	public int insert(WorkEntity dbWork) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.persist(dbWork);
		entityTransaction.commit();
		return 1;
	}

	/**
	 * DBWork を更新します
	 * @param dbWork dbWork
	 * @return 更新件数
	 */
	public int update(WorkEntity dbWork) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.merge(dbWork);
		entityTransaction.commit();
		return 1;
	}

	/**
	 * DBWork を削除します
	 * @param dbWork dbWork
	 * @return 削除件数
	 */
	public int delete(WorkEntity dbWork) {
		EntityManager entityManager = createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.remove(entityManager.find(WorkEntity.class, dbWork.getId()));
		entityTransaction.commit();
		return 1;
	}

}

