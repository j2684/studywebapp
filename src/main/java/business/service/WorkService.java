package business.service;

import java.util.List;

import business.dao.WorkDao;
import business.entity.WorkEntity;

/**
 * {@link DBWorkService}
 */
public class WorkService {

	WorkDao dao = new WorkDao();


	/**
	 * DBWork を全件取得します
	 *
	 * @return DBWorkリスト
	 */
	public List<WorkEntity> findAll() {
		return dao.findAll();
	}


	/**
	 * DBWorkを追加
	 * @return 追加件数
	 */
	public int insert(String name) {

		WorkEntity dbWork = new WorkEntity();
		dbWork.setName(name);
		return dao.insert(dbWork);
	}


	/**
	 * DBWorkを更新
	 * @return 更新件数
	 */
	public int update(int id, String name, int version) {

		WorkEntity dbWork = dao.find(id);

		if(dbWork == null || !dbWork.getVersion().equals(version)) {
			return 0;
		}

		dbWork.setName(name);

		return dao.update(dbWork);
	}


	/**
	 * DBWorkを削除
	 * @return 削除件数
	 */
	public int delete(int id, int version) {

		WorkEntity dbWork = dao.find(id);
		if (dbWork == null || !dbWork.getVersion().equals(version)) {
			return 0;
		}
		return dao.delete(dbWork);
	}

}









