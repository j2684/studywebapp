package business.service;

import java.util.List;

import business.dao.UserDao;
import business.entity.UserEntity;

/**
 * {@link DBWorkService}
 */
public class UserService {

	UserDao dao = new UserDao();


	/**
	 * DBWork を全件取得します
	 *
	 * @return DBWorkリスト
	 */
	public List<UserEntity> findAll() {
		return dao.findAll();
	}


	/**
	 * DBWorkを追加
	 * @return 追加件数
	 */
	public int insert(String name) {

		UserEntity user = new UserEntity();
		user.setName(name);
		return dao.insert(user);
	}


	/**
	 * DBWorkを更新
	 * @return 更新件数
	 */
	public int update(int id, String name) {

		UserEntity user = dao.find(id);

		if(user == null) {
			return 0;
		}

		user.setName(name);

		return dao.update(user);
	}


	/**
	 * DBWorkを削除
	 * @return 削除件数
	 */
	public int delete(int id) {

		UserEntity user = dao.find(id);
		if (user == null) {
			return 0;
		}
		return dao.delete(user);
	}

}









