package business.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "USER")
public class UserEntity implements InterfaceEntity<Integer> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;


	private String name;


	private String pass;

	/**
	 * コンストラクタ
	 */
	public UserEntity() {
	}

	/**
	 * コンストラクタ
	 * @param id ID
	 * @param name Name
	 * @param pass Pass
	 */
	public UserEntity(Integer id, String name, String pass) {
		this.id = id;
		this.name = name;
		this.pass = pass;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * @param pass セットする pass
	 */
	public void String(String pass) {
		this.pass = pass;
	}

}