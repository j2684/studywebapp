# DB作成
CREATE DATABASE WEB_APP_DB;

# ユーザ作成
#CREATE USER 'webapp'@'localhost' IDENTIFIED BY 'webapp';

# ユーザ権限追加
#GRANT ALL PRIVILEGES ON WEB_APP_DB.* TO 'webapp'@'localhost';

# 使用DB指定
USE WEB_APP_DB;

