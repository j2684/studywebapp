<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>

<!doctype html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>JavaDevelop</title>
<link rel="shortcut icon" href="<c:url value="/css/favicon.ico" />">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<link rel="stylesheet" href="<c:url value="/css/bootstrap-icons.css" />">
<link rel="stylesheet" href="<c:url value="/css/styles.css" />">
<link rel="stylesheet" href="<c:url value="/css/style.css" />">
</head>

<body>
	<div class="wrap">

		<jsp:include page="../Header.jsp"></jsp:include>
		<div>
			<input type="button" value="ボタンを押す" onclick="sampleAjax()">
		</div>
		<jsp:include page="../SideBar.jsp"></jsp:include>
		<jsp:include page="../home/homePage.jsp"></jsp:include>
		<jsp:include page="../Footer.jsp"></jsp:include>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="<c:url value="/js/home.js" />"></script>
		<script src="<c:url value="/css/bootstrap.bundle.min.js" />"></script>
		<script src="<c:url value="/css/script.js" />"></script>
	</div>
</body>

</html>