<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>

<!doctype html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>JavaDevelop</title>
<link rel="shortcut icon" href="<c:url value="/css/favicon.ico" />">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap-icons.css" />">
    <link rel="stylesheet" href="<c:url value="/css/styles.css" />">
	<link rel="stylesheet" href="<c:url value="/css/style.css" />">
</head>

<body>

	<jsp:include page="../Header.jsp"></jsp:include>
	<jsp:include page="../SideBar.jsp"></jsp:include>
	<jsp:include page="../work/workPage.jsp">
		<jsp:param name="dbWork" value="${dbWorks}" />
	</jsp:include>
	<jsp:include page="../Footer.jsp"></jsp:include>

	<script src="<c:url value="/assets/bootstrap.bundle.min.js" />"></script>
	<script src="<c:url value="/assets/script.js" />"></script>
</body>

</html>